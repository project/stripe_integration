# Stripe Integration

## Introduction

This module provides integration with Stripe via s service that can be used to perform actions on Stripe.

## Prerequisites
- API keys (Publishable key and the Secret key) from stripe - find keys on [stripe dashboard](https://dashboard.stripe.com)

## Features
The module currently,

* Creates a "Cash Donation" Product on providing API keys

* It saves the IDs of product, price and also the payment link to the configuration for future use

* Provides service with function to create checkout session, which creates a checkout link with custom success URL, that can be used for making the payment

* We are including checkout session id in the success URL

## Roadmap

We are considering to inlude following features in the module

* Make it possible to create and manage products of different types

## Dependencies
* "stripe/stripe-php" package and it is added to requrements in composer.json

*Note: Use composer to add the project to ensure that all the dependencies are installed properly*

## Installation
1. Install the [latest release](https://www.drupal.org/project/stripe_integration
/releases/) of the module
1. Complete the settings form at `/admin/config/system/stripe_integration`

## Maintainers:
* [Shashikanth Palvatla (shashikanth171)](https://www.drupal.org/u/shashikanth171)
