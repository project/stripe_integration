<?php

namespace Drupal\stripe_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\stripe_integration\Services\StripeIntegrationService;
use Drupal\domain\DomainNegotiator;
use Drupal\Core\Logger\LoggerChannelFactory;

use function PHPUnit\Framework\isEmpty;

/**
 * Class StripeIntegrationSettingsForm.
 *
 * @package Drupal\stripe_integration\Form
 */
class StripeIntegrationSettingsForm extends ConfigFormBase {


  /**
   * The Stripe Integration service.
   *
   * @var \Drupal\stripe_integration\Services\StripeIntegrationService
   */
  public $stripeIntegrationService;

  /**
   * The Domain Negotiator.
   *
   * @var \Drupal\domain\DomainNegotiator
   */
  protected $domainNegotiator;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('stripe_integration.stripe_integration_service'),
      $container->get('domain.negotiator'),
      $container->get('logger.factory')
    );
  }

  /**
   * StripeController constructor.
   *
   * @param Drupal\stripe_integration\Services\StripeIntegrationService $stripe_integration_service
   *   The Stripe Integration service.
   * @param \Drupal\domain\DomainNegotiator $domainNegotiator
   *   The Domain Negotiator.
   * @param Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Logger Factory.
   */
  public function __construct(StripeIntegrationService $stripe_integration_service, DomainNegotiator $domainNegotiator, LoggerChannelFactory $logger_factory) {
    $this->stripeIntegrationService = $stripe_integration_service;
    $this->domainNegotiator = $domainNegotiator;
    $this->loggerFactory = $logger_factory->get('stripe_integration');
  }


  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'stripe_integration.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'stripe_integration_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('stripe_integration.settings');

    $currentDomain = $this->domainNegotiator->getActiveDomain();

    $live_test_description = $this->t('<p><strong>Important:</strong> Bear in mind that this configuration will be exported in plain text and likely kept under version control. We recommend providing these settings through your settings.php file, directly on the environment and safe from prying eyes.</p>') . $this->t('<a href=":uri">Stripe dashboard</a>', [':uri' => 'https://dashboard.stripe.com/account/apikeys']);

    $html = $this->t('<p><strong>Note:</strong> To get API keys, after logging in to your Stripe account, ') . $this->t('<a href=":uri">Get Test keys</a>', [':uri' => 'https://dashboard.stripe.com/test/apikeys']) . $this->t(' and ') . $this->t('<a href=":uri">Get Live keys</a>', [':uri' => 'https://dashboard.stripe.com/apikeys']);

    $form['mymarkup'] = [
      '#markup' => $html,
    ];

    $form['environment'] = [
      '#type' => 'radios',
      '#options' => ['test' => $this->t('Test'), 'live' => $this->t('Live')],
      '#title' => $this->t('Environment'),
      '#default_value' => $config->get($currentDomain->id() . '_environment'),
      '#required' => TRUE,
    ];

    $form['apikey_test'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Test'),
      '#description' => $live_test_description,
    ];

    $form['apikey_test']['apikey_public_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Publishable'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['test']['public'],
    ];

    $form['apikey_test']['apikey_secret_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['test']['secret'],
    ];

    $form['apikey_test']['test_product_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product ID'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['test']['product_id'],
      '#attributes' => ['readonly' => 'readonly'],
    ];

    $form['apikey_test']['test_price_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Price ID'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['test']['price_id'],
      '#attributes' => ['readonly' => 'readonly'],
    ];

    $form['apikey_test']['test_payment_link_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment link id'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['test']['payment_link_id'],
      '#attributes' => ['readonly' => 'readonly'],
    ];

    $form['apikey_test']['test_payment_link_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment link URL'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['test']['payment_link_url'],
      '#attributes' => ['readonly' => 'readonly'],
    ];

    $form['apikey_live'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Live'),
      '#description' => $live_test_description,
    ];

    $form['apikey_live']['apikey_public_live'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Publishable'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['live']['public'],
    ];

    $form['apikey_live']['apikey_secret_live'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['live']['secret'],
    ];

    $form['apikey_live']['live_product_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product ID'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['live']['product_id'],
      '#attributes' => ['readonly' => 'readonly'],
    ];

    $form['apikey_live']['live_price_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Price ID'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['live']['price_id'],
      '#attributes' => ['readonly' => 'readonly'],
    ];

    $form['apikey_live']['live_payment_link_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment link id'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['live']['payment_link_id'],
      '#attributes' => ['readonly' => 'readonly'],
    ];

    $form['apikey_live']['live_payment_link_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment link URL'),
      '#default_value' => $config->get($currentDomain->id() . '_apikey')['live']['payment_link_url'],
      '#attributes' => ['readonly' => 'readonly'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $currentDomain = $this->domainNegotiator->getActiveDomain();

    $test_data = [
      'product_id' => $form_state->getValue('test_product_id'),
      'price_id' => $form_state->getValue('test_price_id'),
      'payment_link_id' => $form_state->getValue('test_payment_link_id'),
      'payment_link_url' => $form_state->getValue('test_payment_link_url'),
    ];
    $live_data = [
      'product_id' => $form_state->getValue('live_product_id'),
      'price_id' => $form_state->getValue('live_price_id'),
      'payment_link_id' => $form_state->getValue('live_payment_link_id'),
      'payment_link_url' => $form_state->getValue('live_payment_link_url'),
    ];

    if ($form_state->getValue('apikey_secret_test') !== "") {
      if ($form_state->getValue('test_product_id') == "") {
        $test_data = $this->stripeIntegrationService->create_product_price_payment_link($form_state->getValue('apikey_secret_test'));
      }
    }
    else {
      $test_data = [
        'product_id' => "",
        'price_id' => "",
        'payment_link_id' => "",
        'payment_link_url' => "",
      ];
    }

    if ($form_state->getValue('apikey_secret_live') !== "") {
      if ($form_state->getValue('live_product_id') == "") {
        $live_data = $this->stripeIntegrationService->create_product_price_payment_link($form_state->getValue('apikey_secret_live'));
      }
    }
    else {
      $live_data = [
        'product_id' => "",
        'price_id' => "",
        'payment_link_id' => "",
        'payment_link_url' => "",
      ];
    }

    $this->config('stripe_integration.settings')
      ->set($currentDomain->id() . '_apikey.test.public', $form_state->getValue('apikey_public_test'))
      ->set($currentDomain->id() . '_apikey.test.secret', $form_state->getValue('apikey_secret_test'))
      ->set($currentDomain->id() . '_apikey.test.product_id', $test_data['product_id'])
      ->set($currentDomain->id() . '_apikey.test.price_id', $test_data['price_id'])
      ->set($currentDomain->id() . '_apikey.test.payment_link_id', $test_data['payment_link_id'])
      ->set($currentDomain->id() . '_apikey.test.payment_link_url', $test_data['payment_link_url'])

      ->set($currentDomain->id() . '_apikey.live.public', $form_state->getValue('apikey_public_live'))
      ->set($currentDomain->id() . '_apikey.live.secret', $form_state->getValue('apikey_secret_live'))
      ->set($currentDomain->id() . '_apikey.live.product_id', $live_data['product_id'])
      ->set($currentDomain->id() . '_apikey.live.price_id', $live_data['price_id'])
      ->set($currentDomain->id() . '_apikey.live.payment_link_id', $live_data['payment_link_id'])
      ->set($currentDomain->id() . '_apikey.live.payment_link_url', $live_data['payment_link_url'])

      ->set($currentDomain->id() . '_environment', $form_state->getValue('environment'))
      ->save();
  }

}
