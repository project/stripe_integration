<?php

namespace Drupal\stripe_integration\Services;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Stripe\StripeClient;
use Drupal\domain\DomainNegotiator;

/**
 * Service to integrate Nginx Proxy Manager.
 */
class StripeIntegrationService {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The stripe integration config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $stripeIntegrationConfig;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Domain Negotiator.
   *
   * @var \Drupal\domain\DomainNegotiator
   */
  protected $domainNegotiator;

  /**
   * Constructor for NginxProxyManagerService.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   A config factory for retrieving required config objects.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   A Guzzle client object.
   * @param \Drupal\domain\DomainNegotiator $domainNegotiator
   *   The Domain Negotiator.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ClientInterface $httpClient, DomainNegotiator $domainNegotiator) {
    $this->httpClient = $httpClient;
    $this->configFactory = $configFactory;
    $this->stripeIntegrationConfig = $configFactory->get('stripe_integration.settings');
    $this->domainNegotiator = $domainNegotiator;
  }

  /**
   * Function to get stripe environment.
   *
   * @return string
   *   stripe environment.
   */
  public function get_stripe_environment() {
    return $this->stripeIntegrationConfig->get($this->domainNegotiator->getActiveDomain()->id() . "_environment");
  }

  /**
   * Function to get stripe environment config.
   *
   * @return array
   *   stripe environment config.
   */
  public function get_stripe_environment_config(String $environment) {
    return $this->stripeIntegrationConfig->get($this->domainNegotiator->getActiveDomain()->id() . "_apikey." . $environment);
  }

  /**
   * Function to get current stripe environment config.
   *
   * @return array|string
   *   stripe environment config or config item.
   */
  public function get_current_stripe_environment_config(String $config_item) {
    $environment = $this->get_stripe_environment();
    $config = $this->stripeIntegrationConfig->get($this->domainNegotiator->getActiveDomain()->id() . "_apikey." . $environment);
    if ($config_item !== NULL && isset($config[$config_item])) {
      return $config[$config_item];
    }
    return $config;
  }

  /**
   * Function to create Product, Price and Payment link.
   *
   * @return array
   *   Product, Price and Payment link details.
   */
  public function create_product_price_payment_link(String $stripe_secret) {
    // Create stripe client.
    $stripe = new StripeClient($stripe_secret);

    $product = $stripe->products->create([
      'name' => 'Capital Donation',
      'tax_code' => 'txcd_90000001',
    ]);

    $product_id = $product->values()[0];

    $price = $stripe->prices->create([
      'nickname' => "for capital donation",
      'custom_unit_amount' => [
        'enabled' => TRUE
      ],
      'currency' => 'usd',
      'product' => $product_id,
    ]);

    $price_id = $price->values()[0];

    $base_path = $this->domainNegotiator->getActiveDomain()->get('path');

    $payment_link = $stripe->paymentLinks->create([
      'line_items' =>
      [
        [
          'price' => $price_id,
          'quantity' => 1,
        ],
      ],
      'after_completion' => [
        'type' => 'redirect',
        'redirect' => [
          'url' => $base_path . 'capital-donation/checkout/{CHECKOUT_SESSION_ID}',
        ],
      ],
    ]);

    $payment_link_id = $payment_link->values()[0];
    $payment_link_url = end($payment_link->values());

    return [
      "product_id" => $product_id,
      "price_id" => $price_id,
      "payment_link_id" => $payment_link_id,
      "payment_link_url" => $payment_link_url,
    ];

  }

  /**
   * Function to create Checkout session.
   *
   * @return array
   *   Checkout session ID and URL.
   */
  public function create_checkout_session() {

    // Get stripe secret.
    $stripe_secret = $this->get_current_stripe_environment_config('secret');

    // Get price id.
    $price_id = $this->get_current_stripe_environment_config('price_id');

    // Create stripe client.
    $stripe = new StripeClient($stripe_secret);

    $base_path = $this->domainNegotiator->getActiveDomain()->get('path');

    $checkout_session = $stripe->checkout->sessions->create([
      'success_url' => $base_path . "capital-donation/checkout/success/{CHECKOUT_SESSION_ID}",
      'line_items' => [
        [
          'price' => $price_id,
          'quantity' => 1,
        ],
      ],
      'mode' => 'payment',
    ]);

    $checkout_session_id = $checkout_session->values()[0];
    $checkout_session_url = end($checkout_session->values());

    return [
      "checkout_session_id" => $checkout_session_id,
      "checkout_session_url" => $checkout_session_url,
    ];
  }

  /**
   * Function to get data for updating stripe integratin config.
   *
   * @return array
   *   Prodcut, Price and Payment Link details.
   */
  public function update_config(String $stripe_environment, String $stripe_secret) {
    $data = $this->create_product_price_payment_link($stripe_secret);
    return $data;
  }

}
