<?php

namespace Drupal\stripe_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\stripe_integration\Services\StripeIntegrationService;
use Stripe\StripeClient;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Url;

/**
 * Class StripeController.
 */
class StripeController extends ControllerBase {

  /**
   * The Stripe Integration service.
   *
   * @var \Drupal\stripe_integration\Services\StripeIntegrationService
   */
  public $stripeIntegrationService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('stripe_integration.stripe_integration_service'),
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('logger.factory'),
      $container->get('messenger'),
    );
  }

  /**
   * StripeController constructor.
   *
   * @param Drupal\stripe_integration\Services\StripeIntegrationService $stripe_integration_service
   *   The Stripe Integration service.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param Drupal\Core\Database\Connection $connection
   *   The Database connection.
   * @param Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Logger Factory.
   */
  public function __construct(StripeIntegrationService $stripe_integration_service, EntityTypeManagerInterface $entity_type_manager, Connection $connection, LoggerChannelFactory $logger_factory,  Messenger $messenger) {
    $this->stripeIntegrationService = $stripe_integration_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
    $this->loggerFactory = $logger_factory->get('stripe_integration');
    $this->messenger = $messenger;
  }

  /**
   * Callback function for Stripe Checkout session success URL.
   *
   * @param string $session_id
   *   Checkout session ID.
   *
   * @return array
   *   Markup content.
   */
  public function checkoutSuccess($session_id) {

    // Retrieve amount from checkout session data.

    // Get stripe secret.
    $stripe_secret = $this->stripeIntegrationService->get_current_stripe_environment_config('secret');

    // Create stripe client.
    $stripe = new StripeClient($stripe_secret);

    $checkout_session = $stripe->checkout->sessions->retrieve(
      $session_id,
      []
    );

    $payment_intent = $stripe->paymentIntents->retrieve(
      $checkout_session->toArray()['payment_intent'],
      []
    );

    // Convert to float and divide by 100.
    $amount = (float) $payment_intent->toArray()['amount'] / 100;

    if (!($amount > 0)) {
      $this->loggerFactory->error("Payment not successful");
      $content = 'Please contact site admin, if you have made a payment.';
    }
    else {

      // Save amount and set status in the webform submission.
      // Load the webform submission entity type.
      $submission_storage = $this->entityTypeManager->getStorage('webform_submission');

      $query = $this->connection->select('webform_submission_data')
        ->fields('webform_submission_data', ['sid'])
        ->condition('webform_id', 'capital_donation')
        ->condition('name', 'checkout_session_id')
        ->condition('value', $session_id)
        ->distinct();

      $ids = $query->execute()->fetchCol();

      $submissions = $submission_storage->loadMultiple($ids);

      if ($submissions) {
        foreach ($submissions as $submission_id => $submission) {
          $current_data = $submission->getData();
          if ($current_data['hidden_amount'] == "") {
            $current_data['hidden_amount'] = (string) $amount;
            $current_data['status'] = "payment-received";
            $submission->setData($current_data);
            $submission->save();
          }
        }
        $content = 'Thank you for Donation !';
      }
      else {
        $this->loggerFactory->error("Could not find webform submission with given checkout session id. Please contact site admin.");
        $content = 'Please contact site admin, if you have made a payment.';
      }
    }
    $this->messenger->addMessage($content);
    $url = Url::fromRoute('entity.webform.canonical', ['webform' => 'capital_donation']);
    $redirect = new RedirectResponse($url->toString());
    
    return $redirect;
  }

}
